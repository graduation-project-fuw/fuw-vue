## 开发

```bash
# 将项目导入idea后删除node_modules文件夹

# 安装依赖
npm install --registry=https://registry.npm.taobao.org

# 启动服务
npm run dev
```

本项目基于RuoYi框架

## 发布

```bash
# 构建测试环境
npm run build:stage

# 构建生产环境
npm run build:prod
```

## 项目部署

```bash
# 准备工作
提前下载node.js

# 注意
本项目为前后端分离项目

# 项目部署
将项目导入idea中，然后安装依赖
```


- ECharts官网：https://echarts.apache.org/examples/zh/index.html
- GoEasy官网：https://www.goeasy.io/
- Base64图片在线转换工具：http://tool.chinaz.com/tools/imgtobase/



## 界面展示

<table>
    <tr>
        <td><img src="src/assets/images/admin/%E9%A6%96%E9%A1%B5.png"/></td>
        <td><img src="src/assets/images/admin/%E5%95%86%E5%93%81%E7%BB%9F%E8%AE%A1-%E6%9F%A5%E8%AF%A2.png"/></td>
    </tr>
    <tr>
        <td><img src="src/assets/images/admin/%E7%94%A8%E6%88%B7%E6%9F%A5%E8%AF%A2-%E4%BF%AE%E6%94%B9.png"/></td>
        <td><img src="src/assets/images/admin/%E5%95%86%E5%93%81%E4%BF%A1%E6%81%AF-%E6%9F%A5%E8%AF%A2.png"/></td>
    </tr>
    <tr>
        <td><img src="src/assets/images/admin/%E5%95%86%E5%93%81%E4%BF%A1%E6%81%AF-%E4%BF%AE%E6%94%B9.png"/></td>
        <td><img src="src/assets/images/admin/%E4%BB%BB%E5%8A%A1%E5%88%86%E9%85%8D-%E6%96%B0%E5%A2%9E%E6%88%96%E4%BF%AE%E6%94%B9.png"/></td>
    </tr>
    <tr>
        <td><img src="src/assets/images/admin/%E4%BB%BB%E5%8A%A1%E5%88%86%E9%85%8D-%E6%96%B0%E5%A2%9E-%E6%97%A5%E6%9C%9F%E9%80%89%E6%8B%A9.png"/></td>
        <td><img src="src/assets/images/admin/%E8%B4%AD%E7%89%A9%E8%BD%A6-%E8%B4%AD%E4%B9%B0.png"/></td>
    </tr>
 <tr>
        <td><img src="src/assets/images/admin/%E8%81%8A%E5%A4%A9%E4%BC%9A%E8%AF%9D-%E9%A1%B6%E9%83%A8%E5%BC%B9%E6%A1%86.png"/></td>
        <td><img src="src/assets/images/admin/%E8%AE%A2%E5%8D%95%E4%BF%A1%E6%81%AF-%E8%AF%A6%E6%83%85.png"/></td>
    </tr>
</table>
