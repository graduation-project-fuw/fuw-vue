import request from '@/utils/request'

// 查询消息记录列表
export function listMessage(query) {
  return request({
    url: '/fuw/message/list',
    method: 'get',
    params: query
  })
}

// 查询所有符合要求消息记录列表
export function listMessageAll(query) {
  return request({
    url: '/fuw/message/list/message',
    method: 'get',
    params: query
  })
}

// 查询消息未读人员
export function listUnread(query) {
  return request({
    url: '/fuw/message/list/unread',
    method: 'get',
    params: query
  })
}

// 查询消息记录详细
export function getMessage(uId) {
  return request({
    url: '/fuw/message/' + uId,
    method: 'get'
  })
}

// 上传消息图片
export function addMessageUpload(dataForm) {
  return request({
    url: '/fuw/message/upload',
    method: 'post',
    data: dataForm,
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

// 新增消息记录
export function addMessage(dataForm) {
  return request({
    url: '/fuw/message/add',
    method: 'post',
    data: dataForm,
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

// 修改消息记录
export function updateMessage(dataForm) {
  return request({
    url: '/fuw/message/update',
    method: 'post',
    data: dataForm,
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

// 删除消息记录
export function delMessage(uId) {
  return request({
    url: '/fuw/message/' + uId,
    method: 'delete'
  })
}

// 导出消息记录
export function exportMessage(query) {
  return request({
    url: '/fuw/message/export',
    method: 'get',
    params: query
  })
}
