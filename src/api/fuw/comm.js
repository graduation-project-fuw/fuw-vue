import request from '@/utils/request'

// 查询商品信息列表
export function listComm(query) {
  return request({
    url: '/fuw/comm/list',
    method: 'get',
    params: query
  })
}

// 跟进commList中的commId查询商品详细信息
export function listCarComm(query) {
  return request({
    url: '/fuw/comm/list/car',
    method: 'get',
    params: query
  })
}

// 查询商品信息详细
export function getComm(commId) {
  return request({
    url: '/fuw/comm/' + commId,
    method: 'get'
  })
}

// 新增商品信息
export function addComm(dataForm) {
  return request({
    url: '/fuw/comm/add',
    method: 'post',
    data: dataForm,
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

// 修改商品信息
export function updateComm(dataForm) {
  return request({
    url: '/fuw/comm/update',
    method: 'post',
    data: dataForm,
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

// 删除商品信息
export function delComm(commId) {
  return request({
    url: '/fuw/comm/' + commId,
    method: 'delete'
  })
}

// 导出商品信息
export function exportComm(query) {
  return request({
    url: '/fuw/comm/export',
    method: 'get',
    params: query
  })
}
