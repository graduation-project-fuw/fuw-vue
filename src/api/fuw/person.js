import request from '@/utils/request'

// 查询用户信息列表
export function listPerson(query) {
  return request({
    url: '/fuw/person/list',
    method: 'get',
    params: query
  })
}

// 查询用户信息详细
export function getPerson(personId) {
  return request({
    url: '/fuw/person/' + personId,
    method: 'get'
  })
}

// 新增用户信息
export function addPerson(dataForm) {
  return request({
    url: '/fuw/person/add',
    method: 'post',
    data: dataForm,
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

// 修改用户信息
export function updatePerson(dataForm) {
  return request({
    url: '/fuw/person/update',
    method: 'post',
    data: dataForm,
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

// 删除用户信息
export function delPerson(personId) {
  return request({
    url: '/fuw/person/' + personId,
    method: 'delete'
  })
}

// 导出用户信息
export function exportPerson(query) {
  return request({
    url: '/fuw/person/export',
    method: 'get',
    params: query
  })
}
