import request from '@/utils/request'

// 查询订单信息列表
export function listOrder(query) {
  return request({
    url: '/fuw/order/list',
    method: 'get',
    params: query
  })
}

// 查询订单信息详细
export function getOrder(orderId) {
  return request({
    url: '/fuw/order/' + orderId,
    method: 'get'
  })
}

// 查看订单详情信息
export function detailsOrder(orderId) {
  return request({
    url: '/fuw/order/details/' + orderId,
    method: 'get'
  })
}

// 新增订单信息
export function addOrder(data) {
  return request({
    url: '/fuw/order',
    method: 'post',
    data: data
  })
}

// 修改订单信息
export function updateOrder(data) {
  return request({
    url: '/fuw/order',
    method: 'put',
    data: data
  })
}

// 删除订单信息
export function delOrder(orderId) {
  return request({
    url: '/fuw/order/' + orderId,
    method: 'delete'
  })
}

// 导出订单信息
export function exportOrder(query) {
  return request({
    url: '/fuw/order/export',
    method: 'get',
    params: query
  })
}
