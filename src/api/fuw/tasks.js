import request from '@/utils/request'

// 查询任务记录列表
export function listTasks(query) {
  return request({
    url: '/fuw/tasks/list',
    method: 'get',
    params: query
  })
}

// 查询每日任务
export function findTasksByDate(query) {
  return request({
    url: '/fuw/tasks/list/date',
    method: 'get',
    params: query
  })
}

// 查询医疗工作者下拉框
export function listTasksDay() {
  return request({
    url: '/fuw/tasks/list/day',
    method: 'get'
  })
}


// 查询任务记录详细
export function getTasks(tasksId) {
  return request({
    url: '/fuw/tasks/' + tasksId,
    method: 'get'
  })
}

// 新增任务记录
export function addTasks(data) {
  return request({
    url: '/fuw/tasks',
    method: 'post',
    data: data
  })
}

// 修改任务记录
export function updateTasks(data) {
  return request({
    url: '/fuw/tasks',
    method: 'put',
    data: data
  })
}

// 删除任务记录
export function delTasks(tasksId) {
  return request({
    url: '/fuw/tasks/' + tasksId,
    method: 'delete'
  })
}

// 导出任务记录
export function exportTasks(query) {
  return request({
    url: '/fuw/tasks/export',
    method: 'get',
    params: query
  })
}
