import request from '@/utils/request'

// 查询任务分配列表
export function listTask(query) {
  return request({
    url: '/fuw/task/list',
    method: 'get',
    params: query
  })
}

// 查询医疗工作者下拉框
export function listMedical() {
  return request({
    url: '/fuw/task/list/medical',
    method: 'get'
  })
}

// 查询组别/岗位下拉框
export function listPost() {
  return request({
    url: '/fuw/task/list/post',
    method: 'get'
  })
}

// 查询任务分配详细
export function getTask(taskId) {
  return request({
    url: '/fuw/task/' + taskId,
    method: 'get'
  })
}

// 新增任务分配
export function addTask(dataForm) {
  return request({
    url: '/fuw/task/add',
    method: 'post',
    data: dataForm,
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

// 修改任务分配
export function updateTask(dataForm) {
  return request({
    url: '/fuw/task/update',
    method: 'post',
    data: dataForm,
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

// 修改任务状态
export function updateTaskType(dataForm) {
  return request({
    url: '/fuw/task/update/type',
    method: 'post',
    data: dataForm,
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

// 删除任务分配
export function delTask(taskId) {
  return request({
    url: '/fuw/task/' + taskId,
    method: 'delete'
  })
}

// 导出任务分配
export function exportTask(query) {
  return request({
    url: '/fuw/task/export',
    method: 'get',
    params: query
  })
}
