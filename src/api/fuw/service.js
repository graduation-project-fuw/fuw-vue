import request from '@/utils/request'

// 查询医疗服务列表
export function listService(query) {
  return request({
    url: '/fuw/service/list',
    method: 'get',
    params: query
  })
}

// 查询医疗服务详细
export function getService(serviceId) {
  return request({
    url: '/fuw/service/' + serviceId,
    method: 'get'
  })
}

// 查询用户下拉框
export function listPerson() {
  return request({
    url: '/fuw/service/list/person',
    method: 'get'
  })
}

// 查询医疗工作者下拉框
export function listMedical() {
  return request({
    url: '/fuw/service/list/medical',
    method: 'get'
  })
}

// 新增医疗服务
export function addService(data) {
  return request({
    url: '/fuw/service',
    method: 'post',
    data: data
  })
}

// 修改医疗服务
export function updateService(data) {
  return request({
    url: '/fuw/service',
    method: 'put',
    data: data
  })
}

// 删除医疗服务
export function delService(serviceId) {
  return request({
    url: '/fuw/service/' + serviceId,
    method: 'delete'
  })
}

// 导出医疗服务
export function exportService(query) {
  return request({
    url: '/fuw/service/export',
    method: 'get',
    params: query
  })
}
