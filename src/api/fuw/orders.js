import request from '@/utils/request'

// 查询商品订单信息列表
export function listOrders(query) {
  return request({
    url: '/fuw/orders/list',
    method: 'get',
    params: query
  })
}

// 查询商品订单信息详细
export function getOrders(uId) {
  return request({
    url: '/fuw/orders/' + uId,
    method: 'get'
  })
}

// 新增商品订单信息
export function addOrders(data) {
  return request({
    url: '/fuw/orders',
    method: 'post',
    data: data
  })
}

// 修改商品订单信息
export function updateOrders(data) {
  return request({
    url: '/fuw/orders',
    method: 'put',
    data: data
  })
}

// 删除商品订单信息
export function delOrders(uId) {
  return request({
    url: '/fuw/orders/' + uId,
    method: 'delete'
  })
}

// 导出商品订单信息
export function exportOrders(query) {
  return request({
    url: '/fuw/orders/export',
    method: 'get',
    params: query
  })
}
