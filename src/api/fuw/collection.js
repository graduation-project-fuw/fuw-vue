import request from '@/utils/request'

// 查询收藏列表
export function listCollection(query) {
  return request({
    url: '/fuw/collection/list',
    method: 'get',
    params: query
  })
}

// 查询收藏详细
export function getCollection(uId) {
  return request({
    url: '/fuw/collection/' + uId,
    method: 'get'
  })
}

// 新增收藏
export function addCollection(data) {
  return request({
    url: '/fuw/collection',
    method: 'post',
    data: data
  })
}

// 修改收藏
export function updateCollection(data) {
  return request({
    url: '/fuw/collection',
    method: 'put',
    data: data
  })
}

// 删除收藏
export function delCollection(uId) {
  return request({
    url: '/fuw/collection/' + uId,
    method: 'delete'
  })
}

// 导出收藏
export function exportCollection(query) {
  return request({
    url: '/fuw/collection/export',
    method: 'get',
    params: query
  })
}
