import request from '@/utils/request'

// 查询购物车列表
export function listCar(query) {
  return request({
    url: '/fuw/car/list',
    method: 'get',
    params: query
  })
}

// 查询用户下拉框
export function listPersonId(query) {
  return request({
    url: '/fuw/car/list/personId',
    method: 'get',
    params: query
  })
}

// 查询商品下拉框
export function listCommId(query) {
  return request({
    url: '/fuw/car/list/commId',
    method: 'get',
    params: query
  })
}

// 查询购物车详细
export function getCar(uId) {
  return request({
    url: '/fuw/car/' + uId,
    method: 'get'
  })
}

// 新增购物车
export function addCar(data) {
  return request({
    url: '/fuw/car',
    method: 'post',
    data: data
  })
}

// 修改购物车
export function updateCar(data) {
  return request({
    url: '/fuw/car',
    method: 'put',
    data: data
  })
}

// 新增订单和商品订单信息
export function addOrder(data) {
  return request({
    url: '/fuw/car/add/addOrder',
    method: 'post',
    data: data
  })
}

// 删除购物车
export function delCar(uId) {
  return request({
    url: '/fuw/car/' + uId,
    method: 'delete'
  })
}

// 导出购物车
export function exportCar(query) {
  return request({
    url: '/fuw/car/export',
    method: 'get',
    params: query
  })
}
